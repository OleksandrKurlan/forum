﻿using Forum.DAL.Entities;
using Forum.DAL.EntityFramework;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Forum.DAL.Identity
{
    /// <summary>
    /// ApplicationUserStore class derives form UserStore and presents the current application Store.
    /// </summary>
    public class ApplicationUserStore : UserStore<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>
    {
        /// <summary>
        /// Parameterized constructor receives ForumContext as an argument.
        /// </summary>
        /// <param name="context">ForumContect object.</param>
        public ApplicationUserStore(ForumContext context)
            : base(context)
        {
        }
    }
}
