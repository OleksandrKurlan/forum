﻿using Forum.DAL.Entities;
using Forum.DAL.EntityFramework;
using Forum.DAL.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;

namespace Forum.DAL.Repositories
{
    public class MessageRepository : IRepository<Message>
    {
        private readonly ForumContext _context;

        public MessageRepository(ForumContext forumContext)
        {
            _context = forumContext;
        }

        public void Create(Message item)
        {
            _context.Messages.Add(item);
        }

        public void Delete(int id)
        {
            Message message = _context.Messages.Find(id);
            if (message != null)
            {
                _context.Messages.Remove(message);
            }
        }

        public Message Get(int id)
        {
            return _context.Messages.Find(id);
        }

        public IEnumerable<Message> GetAll()
        {
            return _context.Messages;
        }

        public void Update(Message item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
