﻿using Forum.DAL.Entities;
using Forum.DAL.EntityFramework;
using Forum.DAL.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Forum.DAL.Repositories
{
    public class TopicRepository : IRepository<Topic>
    {
        private readonly ForumContext _context;

        public TopicRepository(ForumContext forumContext)
        {
            _context = forumContext;
        }

        public void Create(Topic item)
        {
            _context.Topics.Add(item);
        }

        public void Delete(int id)
        {
            Topic topic = _context.Topics.Find(id);
            if (topic != null)
            {
                _context.Topics.Remove(topic);
            }
        }

        public Topic Get(int id)
        {
            return _context.Topics.Include(s => s.Messages).FirstOrDefault(r => r.Id == id);
        }

        public IEnumerable<Topic> GetAll()
        {
            return _context.Topics;
        }

        public void Update(Topic item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
