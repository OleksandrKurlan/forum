﻿using Forum.DAL.Entities;
using Forum.DAL.EntityFramework;
using Forum.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.DAL.Repositories
{
    public class ExceptionLogRepository : IExceptionLogRepository
    {
        private readonly ForumContext _context;

        public ExceptionLogRepository(ForumContext forumContext)
        {
            _context = forumContext;
        }
        public void Log(ExceptionLog exceptionLog)
        {
            _context.ExceptionLogs.Add(exceptionLog);
        }
    }
}
