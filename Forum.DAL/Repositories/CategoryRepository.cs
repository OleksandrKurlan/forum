﻿using Forum.DAL.Entities;
using Forum.DAL.EntityFramework;
using Forum.DAL.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Forum.DAL.Repositories
{
    public class CategoryRepository : IRepository<Category>
    {
        private readonly ForumContext _context;

        public CategoryRepository(ForumContext forumContext)
        {
            _context = forumContext;
        }

        public void Create(Category item)
        {
            _context.Categories.Add(item);
        }

        public void Delete(int id)
        {
            Category category = _context.Categories.Find(id);
            if (category != null)
            {
                _context.Categories.Remove(category);
            }
        }

        public Category Get(int id)
        {
            return _context.Categories.Include(s => s.Topics).FirstOrDefault(f => f.Id == id);
        }

        public IEnumerable<Category> GetAll()
        {
            return _context.Categories;
        }

        public void Update(Category item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
