﻿using Forum.DAL.Entities;
using Forum.DAL.EntityFramework;
using Forum.DAL.Identity;
using Forum.DAL.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Threading.Tasks;

namespace Forum.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ForumContext _db;

        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        private IRepository<Topic> _topicRepository;
        private IRepository<Message> _messageRepository;
        private IRepository<Category> _categoryRepository;
        private IExceptionLogRepository _exceptionLogRepository;

        public UnitOfWork(string connectionString)
        {
            _db = new ForumContext(connectionString);
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                if (_userManager == null)
                {
                    _userManager = new ApplicationUserManager(new ApplicationUserStore(_db));
                }
                return _userManager;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                if (_roleManager == null)
                {
                    _roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(_db));
                }
                return _roleManager;
            }
        }

        public IRepository<Topic> Topics
        {
            get
            {
                if (_topicRepository == null)
                {
                    _topicRepository = new TopicRepository(_db);
                }
                return _topicRepository;
            }
        }

        public IRepository<Message> Messages
        {
            get
            {
                if (_messageRepository == null)
                {
                    _messageRepository = new MessageRepository(_db);
                }
                return _messageRepository;
            }
        }

        public IRepository<Category> Categories
        {
            get
            {
                if (_categoryRepository == null)
                {
                    _categoryRepository = new CategoryRepository(_db);
                }
                return _categoryRepository;
            }
        }

        public IExceptionLogRepository ExceptionLogs
        {
            get
            {
                if (_exceptionLogRepository == null)
                {
                    _exceptionLogRepository = new ExceptionLogRepository(_db);
                }
                return _exceptionLogRepository;
            }
        }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                _disposed = true;
            }
        }

    }
}
