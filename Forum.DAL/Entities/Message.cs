﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Forum.DAL.Entities
{
    /// <summary>
    /// The topic message entity with usual for each message fields. Derives from BaseEntity.
    /// </summary>
    public class Message : BaseEntity
    {
        /// <summary>
        /// Foreign key for Author property.
        /// </summary>
        [ForeignKey("Author")]
        public string AuthorId { get; set; }

        /// <summary>
        /// Date of message posting.
        /// </summary>
        public DateTime DateOfPosting { get; set; }

        /// <summary>
        /// Foreign key for Topic property. Refers to the topic which contains the message.
        /// </summary>
        [ForeignKey("Topic")]
        public int TopicId { get; set; }

        /// <summary>
        /// Foreign key for ResponseTo property. Might contains Id of a message which current message is a response to.
        /// </summary>
        [ForeignKey("ResponseTo")]
        public int? ResponseToId { get; set; }

        /// <summary>
        /// Text of the message.
        /// </summary>
        [Required]
        [StringLength(1000)]
        public string Text { get; set; }

        /// <summary>
        /// IsHidden property shows if the message is hidden from feed.
        /// </summary>
        public bool IsHidden { get; set; }



        /// <summary>
        /// Navigation property with a message which the current message is response to. May contains null.
        /// </summary>
        public virtual Message ResponseTo { get; set; }

        /// <summary>
        /// Navigation property with the Topic which contains the current message.
        /// </summary>
        public virtual Topic Topic { get; set; }

        /// <summary>
        /// Navigation property with ApplicationUser entity who left the message.
        /// </summary>
        public virtual ApplicationUser Author { get; set; }
    }
}
