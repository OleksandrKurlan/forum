﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Forum.DAL.Entities
{
    /// <summary>
    /// The Forum Topic entity contains usual fields for each forum topic. Derives from BaseEntity.
    /// </summary>
    public class Topic : BaseEntity
    {
        /// <summary>
        /// Topic title.
        /// </summary>
        [Required]
        [StringLength(1024)]
        public string Title { get; set; }

        /// <summary>
        /// Foreign key for Author property.
        /// </summary>
        [ForeignKey("Author")]
        public string AuthorId { get; set; }

        /// <summary>
        /// Foreign key for Category property.
        /// </summary>
        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        /// <summary>
        /// Date of the Topic creation.
        /// </summary>
        public DateTime DateOfCreation { get; set; }

        /// <summary>
        /// The topic text property.
        /// </summary>
        [Required]
        [StringLength(3000)]
        public string Text { get; set; }

        /// <summary>
        /// IsHidden property shows if the topic is hidden or not.
        /// </summary>
        public bool IsHidden { get; set; }



        /// <summary>
        /// Navigation property with messages related to the topic.
        /// </summary>
        public virtual ICollection<Message> Messages { get; set; }

        /// <summary>
        /// Navigation property with the topic's Author entity.
        /// </summary>
        public virtual ApplicationUser Author { get; set; }

        /// <summary>
        /// Navigation property with a Category which the topic belongs.
        /// </summary>
        public virtual Category Category { get; set; }
    }
}
