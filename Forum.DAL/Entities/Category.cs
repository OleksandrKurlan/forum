﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.DAL.Entities
{
    /// <summary>
    /// The Forum Category contains topics inside. Derives from BaseEntity.
    /// </summary>
    public class Category : BaseEntity
    {
        /// <summary>
        /// Name of the category property.
        /// </summary>
        [Required]
        [StringLength(255)]
        public string Name { get; set; }



        /// <summary>
        /// Navigation property with topics which belong to the current Category.
        /// </summary>
        public virtual ICollection<Topic> Topics { get; set; }
    }
}
