﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Forum.DAL.Entities
{
    /// <summary>
    /// ApplicationUser entity. Extends the IdentityUser entity.
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// Name of the Forum user. Contains unique string value for each user.
        /// </summary>
        [Required]
        [StringLength(255)]
        [Index(IsUnique = true)]
        public string Name { get; set; }
    }
}
