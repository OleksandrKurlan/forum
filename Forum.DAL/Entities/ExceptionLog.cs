﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.DAL.Entities
{
    /// <summary>
    /// ExceptionLog entity. Contains an exception info and location where the exception happened.
    /// </summary>
    public class ExceptionLog
    {
        /// <summary>
        /// Unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Exception message property. Contains value of current exception message.
        /// </summary>
        [StringLength(4000)]
        public string ExceptionMessage { get; set; }

        /// <summary>
        /// Property contains a name of the controller where the exception was thrown.
        /// </summary>
        [StringLength(1000)]
        public string ControllerName { get; set; }

        /// <summary>
        /// Property contains a name of the action where the exception was thrown.
        /// </summary>
        [StringLength(1000)]
        public string ActionName { get; set; }

        /// <summary>
        /// Property contains Stack Trace of the exception.
        /// </summary>
        public string StackTrace { get; set; }

        /// <summary>
        /// Date and time when the exception was thrown.
        /// </summary>
        public DateTime Date { get; set; }
    }
}
