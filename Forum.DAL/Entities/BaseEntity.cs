﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Forum.DAL.Entities
{
    /// <summary>
    /// Base abstract class with int Id property.
    /// </summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// Unique Identifier for entities.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
    }
}
