﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Forum.DAL.Entities
{
    /// <summary>
    /// ApplicationRole entity. Derives from IdentityRole.
    /// </summary>
    public class ApplicationRole : IdentityRole
    {
    }
}
