﻿using Forum.DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace Forum.DAL.EntityFramework
{
    /// <summary>
    /// Forum Db Context derives from IdentityDbContext
    /// </summary>
    public class ForumContext : IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>
    {
        /// <summary>
        /// Represents Topics table.
        /// </summary>
        public DbSet<Topic> Topics { get; set; }

        /// <summary>
        /// Represents Messages table.
        /// </summary>
        public DbSet<Message> Messages { get; set; }

        /// <summary>
        /// Represents Categories table.
        /// </summary>
        public DbSet<Category> Categories { get; set; }

        /// <summary>
        /// Represents ExceptionLogs table.
        /// </summary>
        public DbSet<ExceptionLog> ExceptionLogs { get; set; }

        /// <summary>
        /// Default constructor for working with migrations.
        /// </summary>
        public ForumContext() : base("ForumContext1")
        {
        }

        /// <summary>
        /// Parameterised constructor for Ninject with connection string as an argument.
        /// </summary>
        /// <param name="connectionString">Connection string for connection to the database.</param>
        public ForumContext(string connectionString) : base(connectionString)
        {

        }
    }
}
