﻿using System.Collections.Generic;

namespace Forum.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Returns all items 
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();
        /// <summary>
        /// Returns item by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T Get(int id);
        /// <summary>
        /// Creates new item
        /// </summary>
        /// <param name="item">T item</param>
        void Create(T item);
        /// <summary>
        /// Edits item
        /// </summary>
        /// <param name="item">Existing item</param>
        void Update(T item);
        /// <summary>
        /// Removes item by id
        /// </summary>
        /// <param name="id"></param>
        void Delete(int id);
    }
}