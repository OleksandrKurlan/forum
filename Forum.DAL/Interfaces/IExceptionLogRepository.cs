﻿using Forum.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.DAL.Interfaces
{
    public interface IExceptionLogRepository
    {
        void Log(ExceptionLog exceptionLog);
    }
}
