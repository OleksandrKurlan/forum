﻿using Forum.DAL.Entities;
using Forum.DAL.Identity;
using System;
using System.Threading.Tasks;

namespace Forum.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        ApplicationRoleManager RoleManager { get; }
        IRepository<Topic> Topics { get; }
        IRepository<Message> Messages { get; }
        IRepository<Category> Categories { get; }
        IExceptionLogRepository ExceptionLogs { get; }
        Task SaveAsync();
        void Save();
    }
}
