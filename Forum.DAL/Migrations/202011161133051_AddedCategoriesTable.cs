﻿namespace Forum.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedCategoriesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false, maxLength: 255),
                })
                .PrimaryKey(t => t.Id);

            AddColumn("dbo.Topics", "CategoryId", c => c.Int());
            CreateIndex("dbo.Topics", "CategoryId");
            AddForeignKey("dbo.Topics", "CategoryId", "dbo.Categories", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Topics", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Topics", new[] { "CategoryId" });
            DropColumn("dbo.Topics", "CategoryId");
            DropTable("dbo.Categories");
        }
    }
}
