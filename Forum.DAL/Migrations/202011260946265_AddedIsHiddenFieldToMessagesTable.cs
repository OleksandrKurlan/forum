﻿namespace Forum.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedIsHiddenFieldToMessagesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Messages", "IsHidden", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            DropColumn("dbo.Messages", "IsHidden");
        }
    }
}
