﻿namespace Forum.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedIsHiddenFieldToTopicsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Topics", "IsHidden", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            DropColumn("dbo.Topics", "IsHidden");
        }
    }
}
