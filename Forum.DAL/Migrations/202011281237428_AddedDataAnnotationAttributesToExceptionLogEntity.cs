﻿namespace Forum.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDataAnnotationAttributesToExceptionLogEntity : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ExceptionLogs", "ExceptionMessage", c => c.String(maxLength: 4000));
            AlterColumn("dbo.ExceptionLogs", "ControllerName", c => c.String(maxLength: 1000));
            AlterColumn("dbo.ExceptionLogs", "ActionName", c => c.String(maxLength: 1000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ExceptionLogs", "ActionName", c => c.String());
            AlterColumn("dbo.ExceptionLogs", "ControllerName", c => c.String());
            AlterColumn("dbo.ExceptionLogs", "ExceptionMessage", c => c.String());
        }
    }
}
