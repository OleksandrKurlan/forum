﻿namespace Forum.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedForeignKeyAttributeCategoryIdForTopicTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Topics", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Topics", new[] { "CategoryId" });
            AlterColumn("dbo.Topics", "CategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.Topics", "CategoryId");
            AddForeignKey("dbo.Topics", "CategoryId", "dbo.Categories", "Id", cascadeDelete: true);
        }

        public override void Down()
        {
            DropForeignKey("dbo.Topics", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Topics", new[] { "CategoryId" });
            AlterColumn("dbo.Topics", "CategoryId", c => c.Int());
            CreateIndex("dbo.Topics", "CategoryId");
            AddForeignKey("dbo.Topics", "CategoryId", "dbo.Categories", "Id");
        }
    }
}
