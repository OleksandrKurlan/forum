﻿namespace Forum.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedResponseToColumnToMessagesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Messages", "ResponseToId", c => c.Int());
            CreateIndex("dbo.Messages", "ResponseToId");
            AddForeignKey("dbo.Messages", "ResponseToId", "dbo.Messages", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Messages", "ResponseToId", "dbo.Messages");
            DropIndex("dbo.Messages", new[] { "ResponseToId" });
            DropColumn("dbo.Messages", "ResponseToId");
        }
    }
}
