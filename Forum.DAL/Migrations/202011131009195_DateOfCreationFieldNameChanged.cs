﻿namespace Forum.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class DateOfCreationFieldNameChanged : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Topics", "DateOfCreation", c => c.DateTime(nullable: false));
            DropColumn("dbo.Topics", "DateOfCreating");
        }

        public override void Down()
        {
            AddColumn("dbo.Topics", "DateOfCreating", c => c.DateTime(nullable: false));
            DropColumn("dbo.Topics", "DateOfCreation");
        }
    }
}
