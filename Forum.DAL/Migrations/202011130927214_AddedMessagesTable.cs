﻿namespace Forum.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedMessagesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Messages",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    AuthorId = c.String(maxLength: 128),
                    DateOfPosting = c.DateTime(nullable: false),
                    Text = c.String(nullable: false),
                    Topic_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.AuthorId)
                .ForeignKey("dbo.Topics", t => t.Topic_Id)
                .Index(t => t.AuthorId)
                .Index(t => t.Topic_Id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Messages", "Topic_Id", "dbo.Topics");
            DropForeignKey("dbo.Messages", "AuthorId", "dbo.AspNetUsers");
            DropIndex("dbo.Messages", new[] { "Topic_Id" });
            DropIndex("dbo.Messages", new[] { "AuthorId" });
            DropTable("dbo.Messages");
        }
    }
}
