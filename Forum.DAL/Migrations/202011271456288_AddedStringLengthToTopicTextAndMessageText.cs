﻿namespace Forum.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedStringLengthToTopicTextAndMessageText : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Topics", "Text", c => c.String(nullable: false, maxLength: 3000));
            AlterColumn("dbo.Messages", "Text", c => c.String(nullable: false, maxLength: 1000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Messages", "Text", c => c.String(nullable: false));
            AlterColumn("dbo.Topics", "Text", c => c.String(nullable: false));
        }
    }
}
