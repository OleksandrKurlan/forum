﻿namespace Forum.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedTopicTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Topics",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Title = c.String(maxLength: 1024),
                    AuthorId = c.String(maxLength: 128),
                    DateOfCreating = c.DateTime(nullable: false),
                    Text = c.String(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.AuthorId)
                .Index(t => t.AuthorId);

            DropColumn("dbo.AspNetRoles", "Discriminator");
        }

        public override void Down()
        {
            AddColumn("dbo.AspNetRoles", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            DropForeignKey("dbo.Topics", "AuthorId", "dbo.AspNetUsers");
            DropIndex("dbo.Topics", new[] { "AuthorId" });
            DropTable("dbo.Topics");
        }
    }
}
