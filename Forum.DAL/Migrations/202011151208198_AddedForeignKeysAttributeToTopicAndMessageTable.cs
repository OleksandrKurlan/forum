﻿namespace Forum.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedForeignKeysAttributeToTopicAndMessageTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Messages", "Topic_Id", "dbo.Topics");
            DropIndex("dbo.Messages", new[] { "Topic_Id" });
            RenameColumn(table: "dbo.Messages", name: "Topic_Id", newName: "TopicId");
            AlterColumn("dbo.Messages", "TopicId", c => c.Int(nullable: false));
            CreateIndex("dbo.Messages", "TopicId");
            AddForeignKey("dbo.Messages", "TopicId", "dbo.Topics", "Id", cascadeDelete: true);
        }

        public override void Down()
        {
            DropForeignKey("dbo.Messages", "TopicId", "dbo.Topics");
            DropIndex("dbo.Messages", new[] { "TopicId" });
            AlterColumn("dbo.Messages", "TopicId", c => c.Int());
            RenameColumn(table: "dbo.Messages", name: "TopicId", newName: "Topic_Id");
            CreateIndex("dbo.Messages", "Topic_Id");
            AddForeignKey("dbo.Messages", "Topic_Id", "dbo.Topics", "Id");
        }
    }
}
