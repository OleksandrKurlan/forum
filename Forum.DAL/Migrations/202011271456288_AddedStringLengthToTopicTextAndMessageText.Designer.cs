﻿// <auto-generated />
namespace Forum.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class AddedStringLengthToTopicTextAndMessageText : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedStringLengthToTopicTextAndMessageText));
        
        string IMigrationMetadata.Id
        {
            get { return "202011271456288_AddedStringLengthToTopicTextAndMessageText"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
