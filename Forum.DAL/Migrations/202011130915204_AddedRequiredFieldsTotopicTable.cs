﻿namespace Forum.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedRequiredFieldsTotopicTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Topics", "Title", c => c.String(nullable: false, maxLength: 1024));
            AlterColumn("dbo.Topics", "Text", c => c.String(nullable: false));
        }

        public override void Down()
        {
            AlterColumn("dbo.Topics", "Text", c => c.String());
            AlterColumn("dbo.Topics", "Title", c => c.String(maxLength: 1024));
        }
    }
}
