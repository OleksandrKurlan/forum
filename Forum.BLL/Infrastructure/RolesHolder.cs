﻿namespace Forum.BLL.Infrastructure
{
    /// <summary>
    /// Static class with hodls combinations of roles in the applications.
    /// </summary>
    public static class RolesHolder
    {
        /// <summary>
        /// Admin role.
        /// </summary>
        public const string AdminRole = "admin";

        /// <summary>
        /// Moderator role.
        /// </summary>
        public const string ModeratorRole = "moderator";

        /// <summary>
        /// Use role.
        /// </summary>
        public const string UserRole = "user";

        /// <summary>
        /// Deleted user role.
        /// </summary>
        public const string DeletedRole = "deleted";

        /// <summary>
        /// Blocked user role.
        /// </summary>
        public const string BlockedRole = "blocked";

        /// <summary>
        /// Combination of moderator and admin roles.
        /// </summary>
        public const string ModeratorOrAdmin = ModeratorRole + "," + AdminRole;

        /// <summary>
        /// Combination of user, moderator and admin roles.
        /// </summary>
        public const string UserOrModeratorOrAdmin = UserRole + "," + ModeratorRole + "," + AdminRole;
    }
}