﻿using AutoMapper;
using Forum.DAL.Interfaces;
using Forum.DAL.Repositories;
using Ninject.Modules;

namespace Forum.BLL.Infrastructure
{
    /// <summary>
    /// NinjectModule binds units for BLL.
    /// </summary>
    public class NinjectBllRegistrations : NinjectModule
    {
        private readonly string _connectionString;

        /// <summary>
        /// The parameterized constructor receives a connection string for UnitOfWork as a parameter.
        /// </summary>
        /// <param name="connection">Connection string for UnitOfWork.</param>
        public NinjectBllRegistrations(string connection)
        {
            _connectionString = connection;
        }

        /// <summary>
        /// Load overrided method
        /// </summary>
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument(_connectionString);

            var mapperConfiguration = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapperProfile>());
            Bind<IMapper>().ToConstructor(c => new Mapper(mapperConfiguration)).InSingletonScope();
        }
    }
}
