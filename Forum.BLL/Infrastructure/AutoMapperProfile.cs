﻿using AutoMapper;
using Forum.BLL.DTO;
using Forum.DAL.Entities;
using System.Linq;

namespace Forum.BLL.Infrastructure
{
    /// <summary>
    /// AutoMapper profile
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Message, MessageDTO>()
                .ForMember(p => p.AuthorId, c => c.MapFrom(s => s.AuthorId))
                .ForMember(p => p.AuthorName, c => c.MapFrom(s => s.Author.Name))
                .ForMember(p => p.TopicId, c => c.MapFrom(s => s.Topic.Id))
                .ForMember(p => p.ResponseTo, c => c.MapFrom(s => s.ResponseTo))
                .ForMember(p => p.ResponseToId, c => c.MapFrom(s => s.ResponseToId));

            CreateMap<MessageDTO, Message>()
                .ForMember(p => p.AuthorId, c => c.MapFrom(s => s.AuthorId))
                .ForMember(p => p.Topic, c => c.Ignore())
                .ForMember(p => p.Author, c => c.Ignore())
                .ForMember(p => p.ResponseTo, c => c.MapFrom(s => s.ResponseTo));

            CreateMap<Topic, TopicDTO>()
                .ForMember(p => p.AuthorName, c => c.MapFrom(s => s.Author.Name))
                .ForMember(p => p.Messages, c => c.MapFrom(s => s.Messages.Select(r => r.Id)))
                .ForMember(p => p.Messages, c => c.MapFrom(s => s.Messages))
                .ReverseMap();

            CreateMap<Category, CategoryDTO>()
                .ForMember(p => p.Topics, c => c.MapFrom(s => s.Topics.Select(r => r.Id)))
                .ForMember(p => p.Topics, c => c.MapFrom(s => s.Topics))
                .ReverseMap();

            CreateMap<ApplicationUser, UserDTO>()
                .ForMember(p => p.Password, c => c.Ignore());

            CreateMap<ExceptionLog, ExceptionLogDTO>()
                .ReverseMap();
        }
    }
}
