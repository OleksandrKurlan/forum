﻿namespace Forum.BLL.Infrastructure
{
    /// <summary>
    /// Operation details class which represents a result of operations with the database.
    /// </summary>
    public class OperationDetails
    {
        /// <summary>
        /// The parameterized constructor creates a result of an operation.
        /// </summary>
        /// <param name="succeeded">Is the operation succedeed</param>
        /// <param name="message">Result mesage</param>
        /// <param name="prop">The property where an error happened</param>
        public OperationDetails(bool succeeded, string message, string prop)
        {
            Succeeded = succeeded;
            Message = message;
            Property = prop;
        }
        public bool Succeeded { get; private set; }
        public string Message { get; private set; }
        public string Property { get; private set; }
    }
}
