﻿using AutoMapper;
using Forum.BLL.DTO;
using Forum.BLL.Interfaces;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.BLL.Services
{
    public class ExceptionLogService : IExceptionLogService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;
        public ExceptionLogService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _database = unitOfWork;
            _mapper = mapper;
        }

        /// <summary>
        /// Logs an unhandled exception. 
        /// </summary>
        /// <param name="exceptionLogDTO"></param>
        public void Log(ExceptionLogDTO exceptionLogDTO)
        {
            var exceptionLog = _mapper.Map<ExceptionLog>(exceptionLogDTO);
            _database.ExceptionLogs.Log(exceptionLog);
            _database.Save();
        }

        public void Dispose()
        {
            _database.Dispose();
        }
    }
}
