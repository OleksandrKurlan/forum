﻿using AutoMapper;
using Forum.BLL.DTO;
using Forum.BLL.Exceptions;
using Forum.BLL.Interfaces;
using Forum.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.BLL.Services
{
    public class MessageService : IMessageServiceIncludeHidden
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;
        public MessageService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _database = unitOfWork;
            _mapper = mapper;
        }
        public MessageDTO GetMessage(int id)
        {
            var message = _database.Messages.Get(id);
            if (message.IsHidden)
            {
                throw new HiddenItemException("Message is hidden");
            }
            return _mapper.Map<MessageDTO>(message);
        }

        public async Task HideMessage(int id)
        {
            var message = _database.Messages.Get(id);
            if (message != null)
            {
                if (!message.IsHidden)
                {
                    message.IsHidden = true;
                    _database.Messages.Update(message);
                    await _database.SaveAsync();
                }
            }
            else
            {
                throw new ArgumentNullException("id", "Message is not found");
            }
        }

        public async Task OpenMessage(int id)
        {
            var message = _database.Messages.Get(id);
            if (message != null)
            {
                if (message.IsHidden)
                {
                    message.IsHidden = false;
                    _database.Messages.Update(message);
                    await _database.SaveAsync();
                }
            }
            else
            {
                throw new ArgumentNullException("id", "Message is not found");
            }
        }

        public void Dispose()
        {
            _database.Dispose();
        }

        /// <summary>
        /// Gets a user's message.
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>All user's messages</returns>
        public IEnumerable<MessageDTO> GetMessagesByUser(string userId)
        {
            if (!String.IsNullOrEmpty(userId))
            {
                var messages = _database.Messages.GetAll().Where(w => w.AuthorId == userId && !w.IsHidden);
                return _mapper.Map<IEnumerable<MessageDTO>>(messages);
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        /// <summary>
        /// Removes message by user
        /// </summary>
        /// <param name="messageId">Message id</param>
        /// <param name="authorId">User id</param>
        /// <returns>Task</returns>
        public async Task DeleteMessageAsync(int messageId, string authorId)
        {
            var message = _database.Messages.Get(messageId);
            if (message != null && message.AuthorId == authorId)
            {
                _database.Messages.Delete(messageId);
                await _database.SaveAsync();
            }
        }
    }
}
