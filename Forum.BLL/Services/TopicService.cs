﻿using AutoMapper;
using Forum.BLL.DTO;
using Forum.BLL.Exceptions;
using Forum.BLL.Interfaces;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.BLL.Services
{
    public class TopicService : ITopicServiceIncludeHidden
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;
        public TopicService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _database = unitOfWork;
            _mapper = mapper;
        }

        public void AddMessageToTopic(MessageDTO messageDTO)
        {
            Message message = _mapper.Map<Message>(messageDTO);

            message.Topic = _database.Topics.Get(messageDTO.TopicId);
            message.Author = _database.UserManager.FindById(message.AuthorId);
            message.DateOfPosting = DateTime.Now;
            message.IsHidden = false;

            _database.Messages.Create(message);
            _database.Save();
        }

        public async Task AddMessageToTopicAsync(MessageDTO messageDTO)
        {
            Message message = _mapper.Map<Message>(messageDTO);

            var topic = _database.Topics.Get(messageDTO.TopicId);
            var author = await _database.UserManager.FindByIdAsync(message.AuthorId);
            if (topic != null && author != null)
            {
                if (!topic.IsHidden)
                {
                    message.Topic = topic;
                    message.Author = author;
                    message.DateOfPosting = DateTime.Now;
                    message.IsHidden = false;

                    _database.Messages.Create(message);
                    await _database.SaveAsync();
                }
                else
                {
                    throw new HiddenItemException("Topic is hidden");
                }
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public async Task AddReplyToTopicAsync(MessageDTO response)
        {
            Message messageResponse = _mapper.Map<Message>(response);

            messageResponse.Topic = _database.Topics.Get(response.TopicId);
            messageResponse.Author = await _database.UserManager.FindByIdAsync(response.AuthorId);
            messageResponse.DateOfPosting = DateTime.Now;
            messageResponse.ResponseTo = _database.Messages.Get(response.ResponseToId.Value);
            messageResponse.IsHidden = false;

            _database.Messages.Create(messageResponse);
            await _database.SaveAsync();
        }

        public void AddReplyToTopic(MessageDTO response)
        {
            Message messageResponse = _mapper.Map<Message>(response);

            messageResponse.Topic = _database.Topics.Get(response.TopicId);
            messageResponse.Author = _database.UserManager.FindById(response.AuthorId);
            messageResponse.DateOfPosting = DateTime.Now;
            messageResponse.ResponseTo = _database.Messages.Get(response.ResponseToId.Value);
            messageResponse.IsHidden = false;

            _database.Messages.Create(messageResponse);
            _database.Save();
        }

        public void Create(TopicDTO topicDTO)
        {
            ApplicationUser author = _database.UserManager.FindByEmail(topicDTO.AuthorName);
            Category category = _database.Categories.Get(topicDTO.CategoryId);
            Topic topic = _mapper.Map<Topic>(topicDTO);
            if (author != null && category != null && topic != null)
            {
                topic.DateOfCreation = DateTime.Now;
                topic.Author = author;
                topic.AuthorId = author.Id;
                topic.Category = category;
                topic.CategoryId = category.Id;
                topic.IsHidden = false;
                _database.Topics.Create(topic);
                _database.Save();
            }
            else
            {
                throw new ArgumentException("Incorrect argument", "topicDTO");
            }
        }

        public async Task CreateAsync(TopicDTO topicDTO)
        {
            ApplicationUser author = await _database.UserManager.FindByEmailAsync(topicDTO.AuthorName);
            Category category = _database.Categories.Get(topicDTO.CategoryId);
            Topic topic = _mapper.Map<Topic>(topicDTO);
            if (author != null && category != null && topic != null)
            {
                topic.DateOfCreation = DateTime.Now;
                topic.Author = author;
                topic.AuthorId = author.Id;
                topic.Category = category;
                topic.CategoryId = category.Id;
                topic.IsHidden = false;
                _database.Topics.Create(topic);
                await _database.SaveAsync();
            }
            else
            {
                throw new ArgumentException("Incorrect argument", "topicDTO");
            }
        }

        public IEnumerable<TopicDTO> GetAllTopics()
        {
            return _mapper.Map<IEnumerable<TopicDTO>>(_database.Topics.GetAll().Where(w => !w.IsHidden).ToList());
        }

        public IEnumerable<TopicDTO> GetAllTopicsWithHidden()
        {
            return _mapper.Map<IEnumerable<TopicDTO>>(_database.Topics.GetAll().ToList());
        }

        public IEnumerable<TopicDTO> GetAllTopicsOnlyHidden()
        {
            return _mapper.Map<IEnumerable<TopicDTO>>(_database.Topics.GetAll().Where(w => w.IsHidden).ToList());
        }

        public TopicDTO GetTopic(int id)
        {
            var topic = _database.Topics.Get(id);
            var messgesWithoutHidden = topic.Messages.Where(w => !w.IsHidden).ToList();
            topic.Messages = messgesWithoutHidden;
            if (topic.IsHidden)
            {
                throw new HiddenItemException("Topic is hidden");
            }
            return _mapper.Map<TopicDTO>(topic);
        }

        public TopicDTO GetTopicIncludeHidden(int id)
        {
            var topic = _database.Topics.Get(id);
            return _mapper.Map<TopicDTO>(topic);
        }

        /// <summary>
        /// Returns topics which have updated today.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TopicDTO> GetTodayUpdatedTopics()
        {
            var todayUpdatedTopics = GetAllTopics().Where(r => !r.IsHidden)
                                                   .Where(w => w.DateOfCreation > DateTime.Now.AddDays(-1)
                                                               || w.Messages.Where(s => s.DateOfPosting > DateTime.Now.AddDays(-1)).Any())
                                                   .OrderByDescending(r => r.Messages.Any() ? r.Messages.Last().DateOfPosting : r.DateOfCreation)
                                                   .ToList();
            return _mapper.Map<IEnumerable<TopicDTO>>(todayUpdatedTopics);
        }

        /// <summary>
        /// Returns today updated topics with hidden for moderator.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TopicDTO> GetTodayUpdatedTopicsIncludeHidden()
        {
            var todayUpdatedTopics = GetAllTopicsWithHidden().Where(w => w.DateOfCreation > DateTime.Now.AddDays(-1)
                                                                         || w.Messages.Where(s => s.DateOfPosting > DateTime.Now.AddDays(-1)).Any())
                                                             .OrderByDescending(r => r.Messages.Any() ? r.Messages.Last().DateOfPosting : r.DateOfCreation)
                                                             .ToList();
            return _mapper.Map<IEnumerable<TopicDTO>>(todayUpdatedTopics);
        }

        public async Task HideTopic(int id)
        {
            var topic = _database.Topics.Get(id);
            if (topic != null)
            {
                if (!topic.IsHidden)
                {
                    topic.IsHidden = true;
                    _database.Topics.Update(topic);
                    await _database.SaveAsync();
                }
            }
            else
            {
                throw new ArgumentNullException("id", "Topic is not found");
            }
        }

        public async Task OpenTopic(int id)
        {
            var topic = _database.Topics.Get(id);
            if (topic != null)
            {
                if (topic.IsHidden)
                {
                    topic.IsHidden = false;
                    _database.Topics.Update(topic);
                    await _database.SaveAsync();
                }
            }
            else
            {
                throw new ArgumentNullException("id", "Topic is not found");
            }
        }

        public void Dispose()
        {
            _database.Dispose();
        }
    }
}
