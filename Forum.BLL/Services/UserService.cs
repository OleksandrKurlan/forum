﻿using AutoMapper;
using Forum.BLL.DTO;
using Forum.BLL.Infrastructure;
using Forum.BLL.Interfaces;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Forum.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _database = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ClaimsIdentity> Authenticate(UserDTO userDTO)
        {
            ClaimsIdentity claim = null;
            ApplicationUser user = await _database.UserManager.FindAsync(userDTO.Email, userDTO.Password);
            if (user != null)
            {
                claim = await _database.UserManager.CreateIdentityAsync(
                    user,
                    DefaultAuthenticationTypes.ApplicationCookie);
            }
            return claim;

        }

        public async Task<OperationDetails> Create(UserDTO userDTO)
        {
            ApplicationUser user = await _database.UserManager.FindByEmailAsync(userDTO.Email);
            if (user == null)
            {
                user = new ApplicationUser { Email = userDTO.Email, UserName = userDTO.Email, Name = userDTO.Name };
                var result = await _database.UserManager.CreateAsync(user, userDTO.Password);
                if (result.Errors.Any())
                {
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
                }
                await _database.UserManager.AddToRoleAsync(user.Id, userDTO.Role);
                await _database.SaveAsync();
                return new OperationDetails(true, "Registration successful", "");
            }
            else
            {
                return new OperationDetails(false, "User with this Email is already exists", "Email");
            }

        }

        public void Dispose()
        {
            _database.Dispose();
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            //right outer join for users and roles tables
            var users = from user in _database.UserManager.Users.ToList()
                        join role in _database.RoleManager.Roles.ToList()
                           on user.Roles.Select(s => s.RoleId).LastOrDefault() equals role.Id into usersWithRoles
                        from role in usersWithRoles.DefaultIfEmpty()
                        select new UserDTO
                        {
                            Id = user.Id,
                            Email = user.Email,
                            Name = user.Name,
                            Password = null,
                            UserName = user.UserName,
                            Role = role?.Name ?? String.Empty
                        };


            return users;
        }

        public UserDTO GetUserById(string id)
        {
            var user = GetAllUsers().Where(w => w.Id == id).SingleOrDefault();
            return _mapper.Map<UserDTO>(user);
        }

        public async Task<OperationDetails> DeleteUserByIdAsync(string id)
        {
            var user = await _database.UserManager.FindByIdAsync(id);
            if (user != null)
            {
                var rolesForUser = await _database.UserManager.GetRolesAsync(id);
                if (rolesForUser.Count() > 0)
                {
                    foreach (var item in rolesForUser.ToList())
                    {
                        var result = await _database.UserManager.RemoveFromRoleAsync(user.Id, item);
                        if (result.Errors.Any())
                        {
                            return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
                        }
                    }

                    await _database.SaveAsync();

                    return new OperationDetails(true, "User is successfully deleted from all roles", "");
                }
                else
                {
                    return new OperationDetails(false, "User is already deleted", "");
                }
            }
            else
            {
                return new OperationDetails(false, "User not found", "");
            }
        }

        public OperationDetails DeleteUserById(string id)
        {
            var user = _database.UserManager.FindById(id);
            if (user != null)
            {
                var rolesForUser = _database.UserManager.GetRoles(id);
                if (rolesForUser.Count() > 0)
                {
                    foreach (var item in rolesForUser.ToList())
                    {
                        var result = _database.UserManager.RemoveFromRole(user.Id, item);
                        if (result.Errors.Any())
                        {
                            return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
                        }
                    }

                    _database.Save();

                    return new OperationDetails(true, "User is successfully deleted from all roles", "");
                }
                else
                {
                    return new OperationDetails(false, "User is already deleted", "");
                }
            }
            else
            {
                return new OperationDetails(false, "User not found", "");
            }
        }

        public async Task<OperationDetails> AddToRoleAsync(string userId, string roleName)
        {
            var role = await _database.RoleManager.FindByNameAsync(roleName);
            var user = await _database.UserManager.FindByIdAsync(userId);
            var currentUserRole = await _database.RoleManager.FindByIdAsync(user.Roles.Select(s => s.RoleId).SingleOrDefault());
            if (role != null && user != null)
            {
                if (currentUserRole != null && currentUserRole.Name != RolesHolder.AdminRole)
                {
                    await _database.UserManager.RemoveFromRoleAsync(user.Id, currentUserRole.Name);
                }
                await _database.UserManager.AddToRoleAsync(user.Id, role.Name);
                await _database.SaveAsync();
                return new OperationDetails(true, $"{user.Name} is successfully added to {role.Name} role", "");
            }
            else
            {
                return new OperationDetails(false, "User or role not found", "userId or roleId");
            }
        }

        public UserDTO GetUserByName(string userName)
        {
            var user = GetAllUsers().Where(w => w.Name == userName).SingleOrDefault();
            return _mapper.Map<UserDTO>(user);
        }

        public async Task<OperationDetails> ChangePasswordAsync(string userId, string currentPassword, string newPassword)
        {
            var result = await _database.UserManager.ChangePasswordAsync(userId, currentPassword, newPassword);
            if (result.Errors.Any())
            {
                return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
            }
            await _database.SaveAsync();
            return new OperationDetails(true, "Password is successfully changed", "");
        }

        public async Task<OperationDetails> ChangeNameAsync(string userId, string password, string newName)
        {
            var user = await _database.UserManager.FindByIdAsync(userId);
            if (await _database.UserManager.CheckPasswordAsync(user, password))
            {
                user.Name = newName;
                var result = await _database.UserManager.UpdateAsync(user);
                if (result.Errors.Any())
                {
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
                }
                await _database.SaveAsync();
                return new OperationDetails(true, "Name is successfully changed", "");
            }
            else
            {
                return new OperationDetails(false, "Incorrect password", "");
            }
        }
    }
}
