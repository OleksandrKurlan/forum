﻿using AutoMapper;
using Forum.BLL.DTO;
using Forum.BLL.Interfaces;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.BLL.Services
{
    public class CategoryService : ICategoryServiceIncludeCrud
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;
        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _database = unitOfWork;
            _mapper = mapper;
        }

        public async Task CreateAsync(CategoryDTO categoryDTO)
        {
            Category category = _mapper.Map<Category>(categoryDTO);
            _database.Categories.Create(category);
            await _database.SaveAsync();
        }

        public void Delete(int id)
        {
            _database.Categories.Delete(id);
            _database.Save();
        }

        public async Task DeleteAsync(int id)
        {
            _database.Categories.Delete(id);
            await _database.SaveAsync();
        }

        public IEnumerable<CategoryDTO> GetAllCategories()
        {
            return _mapper.Map<IEnumerable<CategoryDTO>>(_database.Categories.GetAll().ToList());
        }

        public CategoryDTO GetCategory(int id)
        {
            var category = _database.Categories.Get(id);
            var nonHiddenTopics = category.Topics.Where(w => !w.IsHidden).ToList();
            category.Topics = nonHiddenTopics;
            return _mapper.Map<CategoryDTO>(category);
        }

        /// <summary>
        /// Gets a category with hiddent topics for moderator actions.
        /// </summary>
        /// <param name="id">Category id</param>
        /// <returns>Category with hidden topics</returns>
        public CategoryDTO GetCategoryWithHidden(int id)
        {
            var category = _database.Categories.Get(id);
            return _mapper.Map<CategoryDTO>(category);
        }

        public void Update(CategoryDTO categoryDTO)
        {
            if (categoryDTO != null && !String.IsNullOrEmpty(categoryDTO.Name) && categoryDTO.Id > 0)
            {
                var category = _database.Categories.Get(categoryDTO.Id);
                category.Name = categoryDTO.Name;
                _database.Categories.Update(category);
                _database.Save();
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public async Task UpdateAsync(CategoryDTO categoryDTO)
        {
            if (categoryDTO != null && !String.IsNullOrEmpty(categoryDTO.Name) && categoryDTO.Id > 0)
            {
                var category = _database.Categories.Get(categoryDTO.Id);
                category.Name = categoryDTO.Name;
                _database.Categories.Update(category);
                await _database.SaveAsync();
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public void Dispose()
        {
            _database.Dispose();
        }
    }
}
