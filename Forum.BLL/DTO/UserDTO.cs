﻿namespace Forum.BLL.DTO
{
    /// <summary>
    /// Data transfer object of the User entity.
    /// </summary>
    public class UserDTO
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// User's email address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// User's password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Username
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Name of the User
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// User's role
        /// </summary>
        public string Role { get; set; }
    }
}
