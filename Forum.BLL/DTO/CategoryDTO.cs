﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.BLL.DTO
{
    /// <summary>
    /// Data transfer object of Category entity.
    /// </summary>
    public class CategoryDTO
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Category Name
        /// </summary>
        [Required]
        [Display(Name = "Category Name")]
        [StringLength(255, MinimumLength = 2)]
        public string Name { get; set; }

        /// <summary>
        /// Topics which belong to the category.
        /// </summary>
        public ICollection<TopicDTO> Topics { get; set; }
    }
}
