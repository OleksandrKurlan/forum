﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.BLL.DTO
{
    /// <summary>
    /// Data transfer object of the Topic entity
    /// </summary>
    public class TopicDTO
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Topic title
        /// </summary>
        [Required]
        [Display(Name = "Topic Title")]
        [StringLength(250, MinimumLength = 2)]
        public string Title { get; set; }

        /// <summary>
        /// Topic's author name
        /// </summary>
        public string AuthorName { get; set; }

        /// <summary>
        /// Date of the topic creation
        /// </summary>
        public DateTime DateOfCreation { get; set; }

        /// <summary>
        /// Id of the category which the current topic belongs to
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// IsHidden property points if the topic is hidden from feed by a moderator
        /// </summary>
        public bool IsHidden { get; set; }

        /// <summary>
        /// Topic text
        /// </summary>
        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Topic Text")]
        public string Text { get; set; }

        /// <summary>
        /// Messages which belong to the current topic
        /// </summary>
        public ICollection<MessageDTO> Messages { get; set; }
    }
}
