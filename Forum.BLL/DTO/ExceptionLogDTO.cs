﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.BLL.DTO
{
    /// <summary>
    /// Data transfer object of ExceptionLog entity.
    /// </summary>
    public class ExceptionLogDTO
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Exception message
        /// </summary>
        public string ExceptionMessage { get; set; }

        /// <summary>
        /// Controller name
        /// </summary>
        public string ControllerName { get; set; }

        /// <summary>
        /// Action name
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// StackTrace
        /// </summary>
        public string StackTrace { get; set; }

        /// <summary>
        /// Date of exception handling
        /// </summary>
        public DateTime Date { get; set; }
    }
}
