﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.BLL.DTO
{
    /// <summary>
    /// Data transfer object of Message entity.
    /// </summary>
    public class MessageDTO
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Author entity id
        /// </summary>
        public string AuthorId { get; set; }

        /// <summary>
        /// Author name
        /// </summary>
        public string AuthorName { get; set; }

        /// <summary>
        /// Date of message posting
        /// </summary>
        public DateTime DateOfPosting { get; set; }

        /// <summary>
        /// Message text
        /// </summary>
        [Required]
        [Display(Name = "Message")]
        [DataType(DataType.MultilineText)]
        public string Text { get; set; }

        /// <summary>
        /// IsHidden property points if the message is hidden by moderator
        /// </summary>
        public bool IsHidden { get; set; }

        /// <summary>
        /// Id of topic which the message belongs to
        /// </summary>
        public int TopicId { get; set; }

        /// <summary>
        /// Nullable id of the message which the current message is response to
        /// </summary>
        public int? ResponseToId { get; set; }

        /// <summary>
        /// The message data transfer object which the current message is response to
        /// </summary>
        public MessageDTO ResponseTo { get; set; }
    }
}
