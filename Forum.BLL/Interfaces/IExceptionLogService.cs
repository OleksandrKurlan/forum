﻿using Forum.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.BLL.Interfaces
{
    /// <summary>
    /// ExceptionLogService interface.
    /// </summary>
    public interface IExceptionLogService
    {
        void Log(ExceptionLogDTO exceptionLogDTO);
    }
}
