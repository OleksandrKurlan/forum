﻿using Forum.BLL.DTO;
using System.Threading.Tasks;

namespace Forum.BLL.Interfaces
{
    /// <summary>
    /// CategoryService interface for all CRUD operations with categories.
    /// </summary>
    public interface ICategoryServiceIncludeCrud : ICategoryService
    {
        CategoryDTO GetCategoryWithHidden(int id);
        Task CreateAsync(CategoryDTO categoryDTO);
        void Update(CategoryDTO categoryDTO);
        Task UpdateAsync(CategoryDTO categoryDTO);
        void Delete(int id);
        Task DeleteAsync(int id);
    }
}
