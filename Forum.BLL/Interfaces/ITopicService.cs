﻿using Forum.BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Forum.BLL.Interfaces
{
    /// <summary>
    /// TopicService interface for user actions.
    /// </summary>
    public interface ITopicService
    {
        IEnumerable<TopicDTO> GetAllTopics();
        TopicDTO GetTopic(int id);
        Task CreateAsync(TopicDTO topicDTO);
        void Create(TopicDTO topicDTO);
        Task AddMessageToTopicAsync(MessageDTO messageDTO);
        void AddMessageToTopic(MessageDTO messageDTO);
        Task AddReplyToTopicAsync(MessageDTO response);
        void AddReplyToTopic(MessageDTO response);
        IEnumerable<TopicDTO> GetTodayUpdatedTopics();
    }
}
