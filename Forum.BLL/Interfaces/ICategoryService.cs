﻿using Forum.BLL.DTO;
using System.Collections.Generic;

namespace Forum.BLL.Interfaces
{
    /// <summary>
    /// CategoryService interface for only getting categories. 
    /// </summary>
    public interface ICategoryService
    {
        IEnumerable<CategoryDTO> GetAllCategories();
        CategoryDTO GetCategory(int id);
    }
}
