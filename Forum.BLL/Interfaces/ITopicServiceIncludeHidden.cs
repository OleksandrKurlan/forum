﻿using Forum.BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Forum.BLL.Interfaces
{
    /// <summary>
    /// TopicService interface for moderator actions.
    /// </summary>
    public interface ITopicServiceIncludeHidden : ITopicService
    {
        TopicDTO GetTopicIncludeHidden(int id);
        IEnumerable<TopicDTO> GetAllTopicsOnlyHidden();
        IEnumerable<TopicDTO> GetAllTopicsWithHidden();
        Task HideTopic(int id);
        Task OpenTopic(int id);
        IEnumerable<TopicDTO> GetTodayUpdatedTopicsIncludeHidden();
    }
}
