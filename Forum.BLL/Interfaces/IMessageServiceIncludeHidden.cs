﻿using System.Threading.Tasks;

namespace Forum.BLL.Interfaces
{
    /// <summary>
    /// MessageService interface for moderator actions.
    /// </summary>
    public interface IMessageServiceIncludeHidden : IMessageService
    {
        Task HideMessage(int id);
        Task OpenMessage(int id);
    }
}
