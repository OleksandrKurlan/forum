﻿using Forum.BLL.DTO;
using Forum.BLL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Forum.BLL.Interfaces
{
    /// <summary>
    /// UserService interface for admin actions with users.
    /// </summary>
    public interface IUserService : IDisposable
    {
        Task<OperationDetails> Create(UserDTO userDTO);
        Task<ClaimsIdentity> Authenticate(UserDTO userDTO);
        IEnumerable<UserDTO> GetAllUsers();
        UserDTO GetUserById(string id);
        UserDTO GetUserByName(string userName);
        OperationDetails DeleteUserById(string id);
        Task<OperationDetails> DeleteUserByIdAsync(string id);
        Task<OperationDetails> AddToRoleAsync(string userId, string roleName);
        Task<OperationDetails> ChangePasswordAsync(string userId, string currentPassword, string newPassword);
        Task<OperationDetails> ChangeNameAsync(string userId, string password, string newName);
    }
}
