﻿using Forum.BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Forum.BLL.Interfaces
{
    /// <summary>
    /// MessageService interface for user actions.
    /// </summary>
    public interface IMessageService
    {
        MessageDTO GetMessage(int id);
        IEnumerable<MessageDTO> GetMessagesByUser(string userId);
        Task DeleteMessageAsync(int messageId, string authorId);
    }
}
