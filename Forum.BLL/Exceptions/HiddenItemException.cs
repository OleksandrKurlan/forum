﻿using System;

namespace Forum.BLL.Exceptions
{
    /// <summary>
    /// Custom Exception is thrown when a sought item is hidden from the feed.
    /// </summary>
    [Serializable]
    public class HiddenItemException : ApplicationException
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public HiddenItemException() { }

        /// <summary>
        /// Parameterized constructor which receives a string as a parameter.
        /// </summary>
        /// <param name="message">Exception message</param>
        public HiddenItemException(string message) : base(message) { }

        /// <summary>
        /// Parameterized constructor which receives a string and an inner exception as a parameters.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public HiddenItemException(string message, Exception inner)
        : base(message, inner) { }

        /// <summary>
        /// Parameterized constructor which receives SerializationInfo and StreamingContext.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected HiddenItemException(
        System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context)
        : base(info, context) { }
    }
}
