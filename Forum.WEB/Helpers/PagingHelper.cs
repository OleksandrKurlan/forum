﻿using Forum.WEB.Models;
using System;
using System.Text;
using System.Web.Mvc;

namespace Forum.WEB.Helpers
{
    /// <summary>
    /// PagingHelper is extention for HtmlHelper with adds a paggination to the view.
    /// </summary>
    public static class PagingHelper
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html,
        PageInfo pageInfo, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();

            TagBuilder liLeft = new TagBuilder("li");
            liLeft.AddCssClass("page-item");

            TagBuilder aLeft = new TagBuilder("a") { InnerHtml = @"&laquo;" };
            aLeft.AddCssClass("page-link");

            if (pageInfo.PageNumber == 1)
            {
                liLeft.AddCssClass("disabled");
                aLeft.MergeAttribute("href", "#");
            }
            else
            {
                aLeft.MergeAttribute("href", pageUrl(pageInfo.PageNumber - 1));
            }

            liLeft.InnerHtml = aLeft.ToString();
            result.Append(liLeft.ToString());

            for (int i = 1; i <= pageInfo.TotalPages; i++)
            {
                TagBuilder li = new TagBuilder("li");
                li.AddCssClass("page-item");
                TagBuilder a = new TagBuilder("a");
                a.MergeAttribute("href", pageUrl(i));
                a.InnerHtml = i.ToString();

                if (i == pageInfo.PageNumber)
                {
                    li.AddCssClass("active");
                }
                a.AddCssClass("page-link");
                li.InnerHtml = a.ToString();
                result.Append(li.ToString());
            }

            TagBuilder liRight = new TagBuilder("li");
            liRight.AddCssClass("page-item");

            TagBuilder aRight = new TagBuilder("a") { InnerHtml = @"&raquo;" };
            aRight.AddCssClass("page-link");

            if (pageInfo.PageNumber == pageInfo.TotalPages)
            {
                liRight.AddCssClass("disabled");
                aRight.MergeAttribute("href", "#");
            }
            else
            {
                aRight.MergeAttribute("href", pageUrl(pageInfo.PageNumber + 1));
            }

            liRight.InnerHtml = aRight.ToString();
            result.Append(liRight.ToString());

            return MvcHtmlString.Create(result.ToString());
        }
    }
}