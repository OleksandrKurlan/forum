﻿using System.ComponentModel.DataAnnotations;

namespace Forum.WEB.Models
{
    /// <summary>
    /// Login model
    /// </summary>
    public class LoginModel
    {
        [Required]
        [EmailAddress]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

    }
}