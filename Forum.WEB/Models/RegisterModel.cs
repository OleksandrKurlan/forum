﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Forum.WEB.Models
{
    /// <summary>
    /// The model for registration a new user.
    /// </summary>
    public class RegisterModel
    {
        [Required]
        [EmailAddress]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, MinimumLength = 6)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        [Required]
        [StringLength(50,
            MinimumLength = 2,
            ErrorMessage = "Name length must be from 2 to 50 characters")]
        [RegularExpression(@"[0-9A-Za-z_-]*",
            ErrorMessage = @"Title must contain only (0-9A-Za-z_-*) characters")]
        [DisplayName("Name")]
        public string Name { get; set; }
    }
}