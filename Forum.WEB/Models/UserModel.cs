﻿using System.ComponentModel.DataAnnotations;

namespace Forum.WEB.Models
{
    /// <summary>
    /// The model which is used for presenting user.
    /// </summary>
    public class UserModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        [Display(Name = "Username")]
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
    }
}