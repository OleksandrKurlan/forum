﻿using System;

namespace Forum.WEB.Models
{
    /// <summary>
    /// Contains necessary properties for pagination.
    /// </summary>
    public class PageInfo
    {
        /// <summary>
        /// Current page number
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Objects number on the page
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Amount of objects
        /// </summary>
        public int TotalItems { get; set; }

        /// <summary>
        /// Amount of pages
        /// </summary>
        public int TotalPages
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / PageSize); }
        }
    }
}