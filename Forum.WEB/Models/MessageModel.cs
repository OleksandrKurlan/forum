﻿using Forum.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forum.WEB.Models
{
    public class MessageModel
    {
        public int Id { get; set; }
        public string AuthorName { get; set; }

        public DateTime DateOfPosting { get; set; }

        public string Text { get; set; }

        public int TopicId { get; set; }

        public int? ResponseToId { get; set; }

        public MessageDTO ResponseTo { get; set; }
    }
}