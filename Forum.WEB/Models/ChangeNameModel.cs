﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Forum.WEB.Models
{
    /// <summary>
    /// Model for Changing user's name.
    /// </summary>
    public class ChangeNameModel
    {
        [Display(Name = "Old Name")]
        public string OldName { get; set; }
        [Required]
        [StringLength(50,
            MinimumLength = 2,
            ErrorMessage = "Name length must be from 2 to 50 characters")]
        [RegularExpression(@"[0-9A-Za-z_-]*",
            ErrorMessage = @"Title must contain only (0-9A-Za-z_-*) characters")]
        [Display(Name = "New name")]
        [Remote("ValidateNewName", "Manage")]
        public string NewName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}