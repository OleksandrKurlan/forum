﻿using Forum.BLL.DTO;

namespace Forum.WEB.ViewModels
{
    public class Topic_ReplyViewModel
    {
        public MessageDTO TargetMessage { get; set; }
        public TopicDTO Topic { get; set; }
        public MessageDTO message { get; set; }
    }
}