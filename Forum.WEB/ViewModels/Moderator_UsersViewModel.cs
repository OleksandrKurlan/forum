﻿using Forum.WEB.Models;
using System.Collections.Generic;

namespace Forum.WEB.ViewModels
{
    public class Moderator_UsersViewModel
    {
        public IEnumerable<UserModel> Users { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}