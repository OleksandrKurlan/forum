﻿using Forum.BLL.DTO;
using Forum.WEB.Models;
using System.Collections.Generic;

namespace Forum.WEB.ViewModels
{
    /// <summary>
    /// ViewModel for Moderator controller, ModerateToday action.
    /// </summary>
    public class Moderator_ModerateTodayViewModel
    {
        public IEnumerable<TopicDTO> Topics { get; set; }

        public PageInfo PageInfo { get; set; }
    }
}