﻿using Forum.BLL.DTO;
using Forum.WEB.Models;
using System.Collections.Generic;

namespace Forum.WEB.ViewModels
{
    /// <summary>
    /// ViewModel for Home controller, Today action.
    /// </summary>
    public class Home_TodayViewModel
    {
        public IEnumerable<TopicDTO> Topics { get; set; }

        public PageInfo PageInfo { get; set; }
    }
}