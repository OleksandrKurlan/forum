﻿using Forum.BLL.DTO;
using Forum.WEB.Models;

namespace Forum.WEB.ViewModels
{
    public class Moderator_ModerateTopicViewModel
    {
        public TopicDTO Topic { get; set; }

        public PageInfo PageInfo { get; set; }
    }
}