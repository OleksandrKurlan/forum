﻿using Forum.BLL.DTO;
using Forum.WEB.Models;

namespace Forum.WEB.ViewModels
{
    public class Home_TopicViewModel
    {
        public TopicDTO Topic { get; set; }

        public MessageDTO NewMessage { get; set; }

        public PageInfo PageInfo { get; set; }
    }
}