﻿using Forum.BLL.DTO;
using Forum.WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forum.WEB.ViewModels
{
    public class Manage_UserMessagesViewModel
    {
        public IEnumerable<MessageModel> Messages { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}