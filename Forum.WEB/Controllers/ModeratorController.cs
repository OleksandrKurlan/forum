﻿using Forum.BLL.Infrastructure;
using Forum.BLL.Interfaces;
using Forum.WEB.Models;
using Forum.WEB.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Forum.WEB.Controllers
{
    [Authorize(Roles = RolesHolder.ModeratorOrAdmin)]
    public class ModeratorController : Controller
    {
        private readonly ITopicServiceIncludeHidden _topicService;
        private readonly ICategoryServiceIncludeCrud _categoryService;
        private readonly IUserService _userService;
        private readonly IMessageServiceIncludeHidden _messageService;
        public ModeratorController(IUserService userService,
                                   ITopicServiceIncludeHidden topicService,
                                   ICategoryServiceIncludeCrud categoryService,
                                   IMessageServiceIncludeHidden messageService)
        {
            _userService = userService;
            _categoryService = categoryService;
            _topicService = topicService;
            _messageService = messageService;
        }
        public ActionResult Index()
        {
            var currentModeratorId = User.Identity.GetUserId();
            var currentModerator = _userService.GetUserById(currentModeratorId);
            return View(currentModerator);
        }

        private PageInfo GetPageInfo(int page, int totalItems)
        {
            int pageSize = 5;

            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = totalItems };

            if (pageInfo.PageNumber > pageInfo.TotalPages)
            {
                pageInfo.PageNumber = pageInfo.TotalPages;
            }
            else if (pageInfo.PageNumber < 1)
            {
                pageInfo.PageNumber = 1;
            }

            return pageInfo;
        }

        public ActionResult Users(int page = 1)
        {
            var users = _userService.GetAllUsers().Where(w => w.Role != RolesHolder.AdminRole
                                                              && w.Role != RolesHolder.ModeratorRole).ToList();
            PageInfo pageInfo = GetPageInfo(page, users.Count());
            List<UserModel> userModels = new List<UserModel>();
            foreach (var userDTO in users)
            {
                UserModel userModel = new UserModel
                {
                    Email = userDTO.Email,
                    Name = userDTO.Name,
                    Id = userDTO.Id,
                    UserName = userDTO.UserName,
                    Role = userDTO.Role
                };
                userModels.Add(userModel);
            }
            Moderator_UsersViewModel viewModel = new Moderator_UsersViewModel
            {
                Users = userModels,
                PageInfo = pageInfo
            };

            return View(viewModel);
        }

        public ActionResult ModerateCategories()
        {
            var categories = _categoryService.GetAllCategories();
            return View(categories);
        }

        public ActionResult ModerateCategory(int id)
        {
            var category = _categoryService.GetCategoryWithHidden(id);
            if (category != null)
            {
                return View(category);
            }
            else
            {
                return RedirectToAction("NotFound", "Error");
            }
        }

        public ActionResult ModerateTopic(int id, int page = 1)
        {
            var topic = _topicService.GetTopicIncludeHidden(id);

            if (topic != null)
            {
                int totalItems = topic.Messages.Count;

                PageInfo pageInfo = GetPageInfo(page, totalItems);

                var messagesPerPage = topic.Messages.Skip((pageInfo.PageNumber - 1) * pageInfo.PageSize)
                                                    .Take(pageInfo.PageSize)
                                                    .ToList();
                topic.Messages = messagesPerPage;

                Moderator_ModerateTopicViewModel viewModel = new Moderator_ModerateTopicViewModel
                {
                    Topic = topic,
                    PageInfo = pageInfo
                };
                ViewBag.Category = _categoryService.GetCategory(topic.CategoryId);
                return View(viewModel);
            }
            else
            {
                return RedirectToAction("NotFound", "Error");
            }
        }

        public ActionResult ModerateUserPage(string userName)
        {
            if (!String.IsNullOrEmpty(userName))
            {
                var user = _userService.GetUserByName(userName);
                if (user != null)
                {
                    return View(user);
                }
                else
                {
                    return RedirectToAction("NotFound", "Error");
                }
            }
            else
            {
                return RedirectToAction("BadRequest", "Error");
            }
        }

        public async Task<ActionResult> BlockUserByModerator(string userId, string redirectTo)
        {
            if (!String.IsNullOrEmpty(userId))
            {
                string roleName = RolesHolder.BlockedRole;
                var result = await _userService.AddToRoleAsync(userId, roleName);
                if (result.Succeeded)
                {
                    return Redirect(redirectTo);
                }
                else
                {
                    return RedirectToAction("NotFound", "Error");
                }
            }
            else
            {
                return RedirectToAction("BadRequest", "Error");
            }
        }

        public async Task<ActionResult> UnblockUserByModerator(string userId, string redirectTo)
        {
            if (!String.IsNullOrEmpty(userId))
            {
                string roleName = RolesHolder.UserRole;
                var result = await _userService.AddToRoleAsync(userId, roleName);
                if (result.Succeeded)
                {
                    return Redirect(redirectTo);
                }
                else
                {
                    return RedirectToAction("NotFound", "Error");
                }
            }
            else
            {
                return RedirectToAction("BadRequest", "Error");
            }
        }

        public async Task<ActionResult> HideTopic(int id, string redirectTo)
        {
            try
            {
                await _topicService.HideTopic(id);
                return Redirect(redirectTo);
            }
            catch (ArgumentNullException)
            {
                return RedirectToAction("NotFound", "Error");
            }
        }

        public async Task<ActionResult> OpenTopic(int id, string redirectTo)
        {
            try
            {
                await _topicService.OpenTopic(id);
                return Redirect(redirectTo);
            }
            catch (ArgumentNullException)
            {
                return RedirectToAction("NotFound", "Error");
            }
        }

        public ActionResult ModerateToday(int page = 1)
        {
            var todayUpdateTopics = _topicService.GetTodayUpdatedTopicsIncludeHidden();
            PageInfo pageInfo = GetPageInfo(page, todayUpdateTopics.Count());
            Moderator_ModerateTodayViewModel viewModel = new Moderator_ModerateTodayViewModel
            {
                Topics = todayUpdateTopics.Skip((pageInfo.PageNumber - 1) * pageInfo.PageSize)
                                          .Take(pageInfo.PageSize)
                                          .ToList(),
                PageInfo = pageInfo
            };
            return View(viewModel);
        }

        public async Task<ActionResult> HideMessage(int id, string redirectTo)
        {
            try
            {
                await _messageService.HideMessage(id);
                return Redirect(redirectTo);
            }
            catch (ArgumentNullException)
            {
                return RedirectToAction("NotFound", "Error");
            }
        }

        public async Task<ActionResult> OpenMessage(int id, string redirectTo)
        {
            try
            {
                await _messageService.OpenMessage(id);
                return Redirect(redirectTo);
            }
            catch (ArgumentNullException)
            {
                return RedirectToAction("NotFound", "Error");
            }
        }
    }
}