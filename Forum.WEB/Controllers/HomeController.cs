﻿using Forum.BLL.DTO;
using Forum.BLL.Infrastructure;
using Forum.BLL.Interfaces;
using Forum.WEB.Models;
using Forum.WEB.ViewModels;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Forum.WEB.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITopicService _topicService;
        private readonly ICategoryService _categoryService;
        public HomeController(ITopicService topicService, ICategoryService categoryService)
        {
            _topicService = topicService;
            _categoryService = categoryService;
        }

        public ActionResult Index()
        {
            var categories = _categoryService.GetAllCategories();
            return View(categories);
        }

        public ActionResult Category(int id)
        {
            var category = _categoryService.GetCategory(id);
            if (category != null)
            {
                return View(category);
            }
            else
            {
                return RedirectToAction("NotFound", "Error");
            }
        }

        private PageInfo GetPageInfo(int page, int totalItems)
        {
            int pageSize = 10;

            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = totalItems };

            if (pageInfo.PageNumber > pageInfo.TotalPages)
            {
                pageInfo.PageNumber = pageInfo.TotalPages;
            }
            else if (pageInfo.PageNumber < 1)
            {
                pageInfo.PageNumber = 1;
            }

            return pageInfo;
        }

        public ActionResult Topic(int id, int page = 1)
        {
            var topic = _topicService.GetTopic(id);

            if (topic != null)
            {
                int totalItems = topic.Messages.Count;

                PageInfo pageInfo = GetPageInfo(page, totalItems);

                var messagesPerPage = topic.Messages.Skip((pageInfo.PageNumber - 1) * pageInfo.PageSize).Take(pageInfo.PageSize).ToList();
                topic.Messages = messagesPerPage;

                Home_TopicViewModel getTopicVIewModel = new Home_TopicViewModel()
                {
                    Topic = topic,
                    PageInfo = pageInfo,
                    NewMessage = new MessageDTO()
                };
                ViewBag.Category = _categoryService.GetCategory(topic.CategoryId);
                return View(getTopicVIewModel);
            }
            else
            {
                return RedirectToAction("NotFound", "Error");
            }
        }

        [Authorize(Roles = RolesHolder.UserOrModeratorOrAdmin)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> LeaveMessage(MessageDTO NewMessage, int TopicId)
        {
            if (ModelState.IsValid)
            {
                NewMessage.AuthorId = User.Identity.GetUserId();
                NewMessage.AuthorName = User.Identity.GetUserName();
                NewMessage.TopicId = TopicId;
                await _topicService.AddMessageToTopicAsync(NewMessage);
                ModelState.Clear();
                return RedirectToAction("Topic", new { id = TopicId });
            }
            else
            {
                var topic = _topicService.GetTopic(TopicId);
                Home_TopicViewModel getTopicVIewModel = new Home_TopicViewModel()
                {
                    Topic = topic,
                    NewMessage = NewMessage
                };
                return View("Topic", getTopicVIewModel);
            }
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Rules()
        {
            return View();
        }

        public ActionResult Today(int page = 1)
        {
            var todayUpdateTopics = _topicService.GetTodayUpdatedTopics();
            PageInfo pageInfo = GetPageInfo(page, todayUpdateTopics.Count());
            Home_TodayViewModel viewModel = new Home_TodayViewModel
            {
                Topics = todayUpdateTopics.Skip((pageInfo.PageNumber - 1) * pageInfo.PageSize)
                                          .Take(pageInfo.PageSize)
                                          .ToList(),
                PageInfo = pageInfo
            };
            return View(viewModel);
        }
    }
}