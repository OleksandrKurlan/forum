﻿using Forum.BLL.Interfaces;
using Forum.WEB.Models;
using Forum.WEB.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Forum.WEB.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private readonly IUserService _userService;
        private readonly IMessageService _messageService;
        public ManageController(IUserService userService, IMessageService messageService)
        {
            _userService = userService;
            _messageService = messageService;
        }
        public ActionResult Index()
        {
            var currentUserId = User.Identity.GetUserId();
            var currentUser = _userService.GetUserById(currentUserId);
            UserModel userModel = new UserModel
            {
                Id = default,
                Email = currentUser.Email,
                Name = currentUser.Name,
                UserName = currentUser.UserName,
                Role = currentUser.Role
            };
            return View(userModel);
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await _userService.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public JsonResult ValidateNewName(string newName)
        {
            var currentName = _userService.GetUserById(User.Identity.GetUserId()).Name;
            ;
            if (String.IsNullOrEmpty(newName))
            {
                return Json("Please enter a new name.", JsonRequestBehavior.AllowGet);
            }
            else if (newName == currentName)
            {
                return Json("The new name must be different as a current name.", JsonRequestBehavior.AllowGet);
            }
            else if(_userService.GetAllUsers().Select(s => s.Name).Contains(newName))
            {
                return Json("That name already exists.", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ChangeName()
        {
            var user = _userService.GetUserById(User.Identity.GetUserId());
            ChangeNameModel model = new ChangeNameModel
            {
                OldName = user.Name
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeName(ChangeNameModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await _userService.ChangeNameAsync(User.Identity.GetUserId(), model.Password, model.NewName);
            if (result.Succeeded)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError("Password", result.Message);
                return View(model);
            }
        }

        private PageInfo GetPageInfo(int page, int totalItems)
        {
            int pageSize = 10;

            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = totalItems };

            if (pageInfo.PageNumber > pageInfo.TotalPages)
            {
                pageInfo.PageNumber = pageInfo.TotalPages;
            }
            else if (pageInfo.PageNumber < 1)
            {
                pageInfo.PageNumber = 1;
            }

            return pageInfo;
        }

        public ActionResult UserMessages(int page = 1)
        {
            var messageDTOs = _messageService.GetMessagesByUser(User.Identity.GetUserId());
            List<MessageModel> messagesModels = new List<MessageModel>();
            foreach (var messageDTO in messageDTOs)
            {
                messagesModels.Add(new MessageModel
                {
                    Id = messageDTO.Id,
                    AuthorName = messageDTO.AuthorName,
                    DateOfPosting = messageDTO.DateOfPosting,
                    TopicId = messageDTO.TopicId,
                    ResponseToId = messageDTO.ResponseToId,
                    ResponseTo = messageDTO.ResponseTo,
                    Text = messageDTO.Text
                });
            }

            PageInfo pageInfo = GetPageInfo(page, messagesModels.Count());

            Manage_UserMessagesViewModel viewModel = new Manage_UserMessagesViewModel
            {
                Messages = messagesModels.Skip((pageInfo.PageNumber - 1) * pageInfo.PageSize).Take(pageInfo.PageSize).ToList(),
                PageInfo = pageInfo
            };
            
            return View(viewModel);
        }

        [HttpPost]
        public async Task DeleteMessage(int id)
        {
            await _messageService.DeleteMessageAsync(id, User.Identity.GetUserId());
        }
    }
}