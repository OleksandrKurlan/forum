﻿using Forum.BLL.DTO;
using Forum.BLL.Interfaces;
using Forum.WEB.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Forum.WEB.Controllers
{
    [Authorize(Roles = "user,moderator,admin")]
    public class TopicController : Controller
    {
        private readonly ITopicService _topicService;
        private readonly ICategoryService _categoryService;
        private readonly IMessageServiceIncludeHidden _messageService;

        public TopicController(ITopicService topicService, ICategoryService categoryService, IMessageServiceIncludeHidden messageService)
        {
            _topicService = topicService;
            _categoryService = categoryService;
            _messageService = messageService;
        }

        [HttpGet]
        public ActionResult Create(int categoryId)
        {
            var category = _categoryService.GetCategory(categoryId);
            if (category != null)
            {
                ViewBag.Category = category;
                return View(new TopicDTO());
            }
            else
            {
                return RedirectToAction("NotFound", "Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(TopicDTO topic, int categoryId)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var category = _categoryService.GetCategory(categoryId);
                    topic.CategoryId = category.Id;
                    topic.AuthorName = User.Identity.GetUserName();
                    await _topicService.CreateAsync(topic);
                    return RedirectToAction("Category", "Home", new { id = category.Id, categoryName = category.Name });
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                var category = _categoryService.GetCategory(topic.CategoryId);
                ViewBag.Category = category;
                return View(topic);
            }
        }

        [HttpGet]
        public ActionResult Reply(int topicId, int responseToId)
        {
            var targetMessage = _messageService.GetMessage(responseToId);
            var topic = _topicService.GetTopic(topicId);

            if (targetMessage != null && topic != null)
            {
                Topic_ReplyViewModel viewModel = new Topic_ReplyViewModel
                {
                    TargetMessage = targetMessage,
                    Topic = topic,
                    message = new MessageDTO()
                };

                var category = _categoryService.GetCategory(topic.CategoryId);
                ViewBag.Category = category;
                return View(viewModel);
            }
            else
            {
                return RedirectToAction("BadRequest", "Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Reply(MessageDTO message, int topicId, int responseToId)
        {
            var topic = _topicService.GetTopic(topicId);
            var category = _categoryService.GetCategory(topic.CategoryId);
            var target = _messageService.GetMessage(responseToId);

            if (topic != null && category != null && target != null)
            {
                if (!ModelState.IsValid)
                {
                    ViewBag.Category = category;
                    Topic_ReplyViewModel viewModel = new Topic_ReplyViewModel
                    {
                        TargetMessage = target,
                        Topic = topic,
                        message = message
                    };
                    return View(viewModel);
                }
                else
                {
                    message.ResponseToId = target.Id;
                    message.AuthorId = User.Identity.GetUserId();
                    message.TopicId = topic.Id;
                    await _topicService.AddMessageToTopicAsync(message);
                    return RedirectToAction("Topic", "Home", new { id = topic.Id });
                }
            }
            else
            {
                return RedirectToAction("BadRequest", "Error");
            }
        }
    }
}