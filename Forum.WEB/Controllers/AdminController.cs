﻿using Forum.BLL.DTO;
using Forum.BLL.Infrastructure;
using Forum.BLL.Interfaces;
using Forum.WEB.Models;
using Forum.WEB.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Forum.WEB.Controllers
{
    [Authorize(Roles = RolesHolder.AdminRole)]
    public class AdminController : Controller
    {
        private readonly IUserService _userService;
        private readonly ICategoryServiceIncludeCrud _categoryService;
        public AdminController(IUserService userService, ICategoryServiceIncludeCrud categoryService)
        {
            _userService = userService;
            _categoryService = categoryService;
        }

        public ActionResult Index()
        {
            var admin = _userService.GetUserById(User.Identity.GetUserId());
            return View(admin);
        }

        private PageInfo GetPageInfo(int page, int totalItems)
        {
            int pageSize = 5;

            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = totalItems };

            if (pageInfo.PageNumber > pageInfo.TotalPages)
            {
                pageInfo.PageNumber = pageInfo.TotalPages;
            }
            else if (pageInfo.PageNumber < 1)
            {
                pageInfo.PageNumber = 1;
            }

            return pageInfo;
        }

        public ActionResult Users(int page = 1)
        {
            var users = _userService.GetAllUsers().ToList();
            PageInfo pageInfo = GetPageInfo(page, users.Count());
            List<UserModel> userModels = new List<UserModel>();
            foreach (var userDTO in users)
            {
                UserModel userModel = new UserModel
                {
                    Email = userDTO.Email,
                    Name = userDTO.Name,
                    Id = userDTO.Id,
                    UserName = userDTO.UserName,
                    Role = userDTO.Role
                };
                userModels.Add(userModel);
            }
            Admin_UsersViewModel viewModel = new Admin_UsersViewModel
            {
                Users = userModels,
                PageInfo = pageInfo
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult DeleteUser(string id)
        {
            var user = _userService.GetUserById(id);
            if (user == null)
            {
                return RedirectToAction("NotFound", "Error");
            }
            else
            {
                OperationDetails result = _userService.DeleteUserById(id);

                if (!result.Succeeded)
                {
                    return RedirectToAction("BadRequest", "Error");

                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }

        public async Task<ActionResult> AssignAsUser(string userId)
        {
            if (!String.IsNullOrEmpty(userId))
            {
                string roleName = RolesHolder.UserRole;
                return await AssignToRole(userId, roleName);
            }
            else
            {
                return RedirectToAction("BadRequest", "Error");
            }
        }

        public async Task<ActionResult> AssignAsModerator(string userId)
        {
            if (!String.IsNullOrEmpty(userId))
            {
                string roleName = RolesHolder.ModeratorRole;
                return await AssignToRole(userId, roleName);
            }
            else
            {
                return RedirectToAction("BadRequest", "Error");
            }
        }

        public async Task<ActionResult> BlockUser(string userId)
        {
            if (!String.IsNullOrEmpty(userId))
            {
                string roleName = RolesHolder.BlockedRole;
                return await AssignToRole(userId, roleName);
            }
            else
            {
                return RedirectToAction("BadRequest", "Error");
            }
        }

        public async Task<ActionResult> UnblockUser(string userId)
        {
            if (!String.IsNullOrEmpty(userId))
            {
                string roleName = RolesHolder.UserRole;
                return await AssignToRole(userId, roleName);
            }
            else
            {
                return RedirectToAction("BadRequest", "Error");
            }
        }

        private async Task<ActionResult> AssignToRole(string userId, string roleName)
        {
            var result = await _userService.AddToRoleAsync(userId, roleName);
            if (result.Succeeded)
            {
                return RedirectToAction("Users");
            }
            else
            {
                return RedirectToAction("NotFound", "Error");
            }
        }

        public ActionResult Categories()
        {
            var categories = _categoryService.GetAllCategories();
            return View(categories);
        }

        [HttpGet]
        public ActionResult CreateCategory()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> CreateCategory(CategoryDTO categoryDTO)
        {
            if (ModelState.IsValid)
            {
                await _categoryService.CreateAsync(categoryDTO);
                return RedirectToAction("Categories");
            }
            return View(categoryDTO);
        }

        public ActionResult EditCategory(int id)
        {
            var category = _categoryService.GetCategory(id);
            return View(category);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> EditCategory(CategoryDTO categoryDTO)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _categoryService.UpdateAsync(categoryDTO);
                }
                catch (ArgumentException)
                {
                    return RedirectToAction("BadRequest", "Error");
                }
            }
            return RedirectToAction("Categories", "Admin");
        }

        public ActionResult DetailsCategory(int id)
        {
            var category = _categoryService.GetCategory(id);
            return View(category);
        }

        public async Task<ActionResult> DeleteCategory(int id)
        {
            var category = _categoryService.GetCategory(id);
            if (category != null)
            {
                await _categoryService.DeleteAsync(id);
                return RedirectToAction("Categories");
            }
            else
            {
                return RedirectToAction("NotFound", "Error");
            }
        }
    }
}