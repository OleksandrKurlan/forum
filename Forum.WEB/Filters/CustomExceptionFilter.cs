﻿using Forum.BLL.DTO;
using Forum.BLL.Interfaces;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Forum.WEB.Filters
{
    /// <summary>
    /// Global custom filter which handles unhandled exceptions and logs those exceptions.
    /// </summary>
    public class CustomExceptionFilter : FilterAttribute, IExceptionFilter
    {
        private readonly IExceptionLogService _logService;

        public CustomExceptionFilter(IExceptionLogService exceptionLogService)
        {
            _logService = exceptionLogService;
        }

        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                ExceptionLogDTO exceptionLog = new ExceptionLogDTO
                {
                    ExceptionMessage = filterContext.Exception.Message,
                    StackTrace = filterContext.Exception.StackTrace,
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    ActionName = filterContext.RouteData.Values["action"].ToString(),
                    Date = DateTime.Now
                };

                _logService.Log(exceptionLog);

                filterContext.Result = new RedirectResult("/Error/NotFound");
                filterContext.ExceptionHandled = true;
            }
        }
    }
}