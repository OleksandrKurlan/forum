﻿using Forum.BLL.Infrastructure;
using Forum.BLL.Interfaces;
using Ninject;
using Ninject.Modules;

namespace Forum.WEB.Infrastructure
{
    /// <summary>
    /// KernelHodlder class contains a StandardKernel with NinjectModules for the entire application.
    /// </summary>
    public class KernelHolder
    {
        static StandardKernel _kernel;
        public static StandardKernel Kernel
        {
            get
            {
                if (_kernel == null)
                {
                    NinjectModule bllNinnjectModule = new NinjectBllRegistrations("ForumContext1");
                    NinjectModule webNinjectModule = new NinjectWebRegistrations();
                    _kernel = new StandardKernel(bllNinnjectModule, webNinjectModule);
                }
                return _kernel;
            }
        }

        /// <summary>
        /// Returns IUserService binded implementation.
        /// </summary>
        /// <returns>IUserService</returns>
        public static IUserService GetUserService()
        {
            return Kernel.Get<IUserService>();
        }

    }
}