﻿using Forum.BLL.Interfaces;
using Forum.BLL.Services;
using Forum.WEB.Filters;
using Ninject.Modules;
using Ninject.Web.Mvc.FilterBindingSyntax;
using System.Web.Mvc;

namespace Forum.WEB.Infrastructure
{
    /// <summary>
    /// NinjectWebRegistrations derives from NinjectModule. The class declares bindings for WEB-layer.
    /// </summary>
    public class NinjectWebRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserService>().To<UserService>();
            Bind<ITopicService>().To<TopicService>();
            Bind<ITopicServiceIncludeHidden>().To<TopicService>();
            Bind<ICategoryService>().To<CategoryService>();
            Bind<ICategoryServiceIncludeCrud>().To<CategoryService>();
            Bind<IMessageService>().To<MessageService>();
            Bind<IMessageServiceIncludeHidden>().To<MessageService>();
            Bind<IExceptionLogService>().To<ExceptionLogService>();
            this.BindFilter<CustomExceptionFilter>(FilterScope.Global, 0);
        }
    }
}