﻿using Forum.WEB.Filters;
using Forum.WEB.Infrastructure;
using System.Web.Mvc;

namespace Forum.WEB
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
